﻿using AForge.Video;
using AForge.Video.DirectShow;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PIA.UC
{
    public partial class Camera : UserControl
    {
        private FilterInfoCollection filter;

        public static VideoCaptureDevice webcam;
        enum FiltersID
        {
            none,
            gray,
            sepia,
            negative,
            opacity,
            brightness,
            contrast,
            binary,
            pixel
        }

        private FiltersID currentFilter=FiltersID.none;
        public Camera()
        {
            InitializeComponent();
        }
        private static List<Color> colorList = new List<Color>();
        private void Camera_Load(object sender, EventArgs e)
        {
            filter = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo device in filter)
            {
                cbDispositivos.Items.Add(device.Name);
            }
            cbDispositivos.SelectedIndex = 0;
            webcam = new VideoCaptureDevice();
           
        }
        static readonly CascadeClassifier cascadeClassifier = new CascadeClassifier("haarcascade_frontalface_alt_tree.xml");
        private void btnPrenderCamara_Click(object sender, EventArgs e)
        {
            webcam = new VideoCaptureDevice(filter[cbDispositivos.SelectedIndex].MonikerString);
            webcam.NewFrame += New_Frame;
            VideoCapabilities[] videoCapabilities;
            videoCapabilities = webcam.VideoCapabilities;
            webcam.VideoResolution = videoCapabilities[0];
            webcam.Start();
        }
        private void New_Frame(object sender, NewFrameEventArgs eventArgs)
        {

            Bitmap bitmap = (Bitmap)eventArgs.Frame.Clone();
            Image<Bgr, byte> grayImage = new Image<Bgr, byte>(bitmap);
            Rectangle[] rectangles = cascadeClassifier.DetectMultiScale(grayImage, 1.2, 1);

            int i = 0;
            if (colorList.Count < rectangles.Length)
                foreach (Rectangle rectangle in rectangles)
                {
                    var rand = new Random();
                    colorList.Add(Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256)));

                }

            Color[] colors = colorList.ToArray();
            foreach (Rectangle rectangle in rectangles)
            {
               

                // Create font and brush.
                Font drawFont = new Font("Arial", 25);
               

                // Set format of string
                
                StringFormat drawFormat = new StringFormat();
                drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    using (Pen pen = new Pen(colors[i], 2))
                    {
                        SolidBrush drawBrush = new SolidBrush(colors[i++]);
                        graphics.DrawRectangle(pen, rectangle);
                        graphics.DrawString(i.ToString(), drawFont, drawBrush, rectangle.X, rectangle.Y-10, drawFormat);
                    }
                }
            }

          
            switch (currentFilter)
            {
                case FiltersID.none:
                    pbCamara.Image = bitmap;
                    break;
                case FiltersID.gray:
                    pbCamara.Image = Filters.GrayScaleImage(bitmap);
                    break;
                case FiltersID.sepia:
                    pbCamara.Image = Filters.Sepia(bitmap);
                    break;
                case FiltersID.negative:
                    pbCamara.Image = Filters.Negative(bitmap);
                    break;
                case FiltersID.opacity:
                    pbCamara.Image = Filters.Alpha(bitmap, (byte)opacityTracker.Value);
                    break;
                case FiltersID.brightness:
                    pbCamara.Image = Filters.Brightness(bitmap, brightnessTracker.Value);
                    break;
                case FiltersID.contrast:
                    pbCamara.Image=Filters.Contrast(bitmap, contrastTracker.Value);
                    break;
                case FiltersID.binary:
                    pbCamara.Image = Filters.Binary(bitmap);
                    break;
                case FiltersID.pixel:
                    pbCamara.Image = Filters.Pixel(bitmap);
                    break;
                default:
                    pbCamara.Image = bitmap;
                    break;
            }
          
        }
        private void Camera_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (webcam.IsRunning)
                webcam.Stop();
        }

        private void btnGrayScale_Click(object sender, EventArgs e)
        {
            currentFilter = FiltersID.gray;
        }

        private void btnSepia_Click(object sender, EventArgs e)
        {
            currentFilter = FiltersID.sepia;
        }

        private void btnNegative_Click(object sender, EventArgs e)
        {
            currentFilter = FiltersID.negative;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            currentFilter = FiltersID.none;
            brightnessTracker.Value = 0;
            contrastTracker.Value = 0;
            opacityTracker.Value = 255;
        }

        private void opacityTracker_ValueChanged(object sender, EventArgs e)
        {
            currentFilter=FiltersID.opacity;
        }

        private void brightnessTracker_ValueChanged(object sender, EventArgs e)
        {
            currentFilter = FiltersID.brightness;
        }

        private void contrastTracker_ValueChanged(object sender, EventArgs e)
        {
            currentFilter = FiltersID.contrast;
        }

        private void btnBinary_Click(object sender, EventArgs e)
        {
            currentFilter = FiltersID.binary;
        }

        private void BTN_PixelFilter_Click(object sender, EventArgs e)
        {
            currentFilter = FiltersID.pixel;
        }
    }
}
