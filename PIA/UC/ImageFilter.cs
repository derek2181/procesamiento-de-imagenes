﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PIA.UC
{
    public partial class ImageFilter : UserControl
    {
        System.Drawing.Image file;
        Boolean opened = false;
        public ImageFilter()
        {
            InitializeComponent();
        }
      

        private void Reload()
        {
            if (!opened)
            {
                // MessageBox.Show("Open an Image then apply changes");
            }
            else
            {
                if (opened)
                {
                    file = System.Drawing.Image.FromFile(openFileDialog1.FileName);
                    pbImage.Image = file;
                    opened = true;
                }
            }
            brightnessTracker.Value = 0;
            contrastTracker.Value = 0;
            opacityTracker.Value = 255;
        }
        private void OpenImage()
        {
            DialogResult dr = openFileDialog1.ShowDialog();

            if (dr == DialogResult.OK)
            {
                file = System.Drawing.Image.FromFile(openFileDialog1.FileName);
                pbImage.Image = file;
                opened = true;
            }
        }
        private void SaveImage()
        {
            if (opened)
            {
                SaveFileDialog sfd = new SaveFileDialog(); // create a new save file dialog object
                sfd.Filter = "Images|*.png;*.bmp;*.jpg";
                ImageFormat format = ImageFormat.Png;// you want to store it in by default format

                if (sfd.ShowDialog()== System.Windows.Forms.DialogResult.OK)
                { 
                string ext =Path.GetExtension(sfd.FileName);
                switch (ext)
                {
                    case ".jpg":
                        format = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        format = ImageFormat.Bmp;
                        break;
                }
                    pbImage.Image.Save(sfd.FileName, format);
                }
                else
                {
                    MessageBox.Show("No haz cargado una Imagen");
                }
            }

        }

      

      
        private void btnUploadFile_Click(object sender, EventArgs e)
        {
            OpenImage();
        }

        private void btnSaveImage_Click(object sender, EventArgs e)
        {
            SaveImage();
        }

       
        private void btnReset_Click(object sender, EventArgs e)
        {
            Reload();
        }

        private void btnGrayScale_Click(object sender, EventArgs e)
        {
            Reload();
            pbImage.Image = Filters.GrayScaleImage(pbImage.Image);
        }

        private void btnSepia_Click(object sender, EventArgs e)
        {
            Reload();
            pbImage.Image = Filters.Sepia(pbImage.Image);
        }

        private void btnNegative_Click(object sender, EventArgs e)
        {

            Reload();
            pbImage.Image = Filters.Negative(pbImage.Image);
        }

        private void opacityTracker_ValueChanged(object sender, EventArgs e)
        {
            if (opened)
            {
                
                percentage.Text = opacityTracker.Value.ToString();

                pbImage.Image = Filters.Alpha(file, (byte)opacityTracker.Value);
            }
            else
            {
                MessageBox.Show("No haz cargado una Imagen");
            }

        }

        private void brightnessTracker_ValueChanged(object sender, EventArgs e)
        {

            if (opened)
            {
                
                brightness.Text = brightnessTracker.Value.ToString();

                pbImage.Image = Filters.Brightness(file,brightnessTracker.Value);
            }
            else
            {
                MessageBox.Show("No haz cargado una Imagen");
            }
        }

        private void contrastTracker_ValueChanged(object sender, EventArgs e)
        {
            if (opened)
            {
                
                contrast.Text =contrastTracker.Value.ToString();

                pbImage.Image = Filters.Contrast((Bitmap)file.Clone(), contrastTracker.Value);
            }
            else
            {
                MessageBox.Show("No haz cargado una Imagen");
            }
        }

        private void contrastTracker_ValueChanged_1(object sender, EventArgs e)
        {

        }

        private void btnColdFilter_Click(object sender, EventArgs e)
        {
            Reload();
            pbImage.Image = Filters.Binary(pbImage.Image);
        }

        private void BTN_Pixel_Click(object sender, EventArgs e)
        {
            Reload();
            pbImage.Image = Filters.Pixel(pbImage.Image);
        }
    }

}
