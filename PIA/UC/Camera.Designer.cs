﻿
namespace PIA.UC
{
    partial class Camera
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbDispositivos = new Guna.UI2.WinForms.Guna2ComboBox();
            this.pbCamara = new Guna.UI2.WinForms.Guna2PictureBox();
            this.contrast = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.contrastTracker = new Guna.UI2.WinForms.Guna2TrackBar();
            this.brightness = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.brightnessTracker = new Guna.UI2.WinForms.Guna2TrackBar();
            this.percentage = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.opacityTracker = new Guna.UI2.WinForms.Guna2TrackBar();
            this.btnNegative = new Guna.UI2.WinForms.Guna2Button();
            this.btnSepia = new Guna.UI2.WinForms.Guna2Button();
            this.btnGrayScale = new Guna.UI2.WinForms.Guna2Button();
            this.btnReset = new Guna.UI2.WinForms.Guna2Button();
            this.btnPrenderCamara = new Guna.UI2.WinForms.Guna2Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBinary = new Guna.UI2.WinForms.Guna2Button();
            this.BTN_PixelFilter = new Guna.UI2.WinForms.Guna2Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbCamara)).BeginInit();
            this.SuspendLayout();
            // 
            // cbDispositivos
            // 
            this.cbDispositivos.BackColor = System.Drawing.Color.Transparent;
            this.cbDispositivos.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbDispositivos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDispositivos.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cbDispositivos.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cbDispositivos.FocusedState.Parent = this.cbDispositivos;
            this.cbDispositivos.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cbDispositivos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.cbDispositivos.HoverState.Parent = this.cbDispositivos;
            this.cbDispositivos.ItemHeight = 30;
            this.cbDispositivos.ItemsAppearance.Parent = this.cbDispositivos;
            this.cbDispositivos.Location = new System.Drawing.Point(184, 18);
            this.cbDispositivos.Name = "cbDispositivos";
            this.cbDispositivos.ShadowDecoration.Parent = this.cbDispositivos;
            this.cbDispositivos.Size = new System.Drawing.Size(140, 36);
            this.cbDispositivos.TabIndex = 0;
            // 
            // pbCamara
            // 
            this.pbCamara.ImageRotate = 0F;
            this.pbCamara.Location = new System.Drawing.Point(30, 74);
            this.pbCamara.Name = "pbCamara";
            this.pbCamara.ShadowDecoration.Parent = this.pbCamara;
            this.pbCamara.Size = new System.Drawing.Size(628, 443);
            this.pbCamara.TabIndex = 2;
            this.pbCamara.TabStop = false;
            // 
            // contrast
            // 
            this.contrast.AutoSize = true;
            this.contrast.Font = new System.Drawing.Font("Techno", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contrast.Location = new System.Drawing.Point(950, 548);
            this.contrast.Name = "contrast";
            this.contrast.Size = new System.Drawing.Size(22, 20);
            this.contrast.TabIndex = 34;
            this.contrast.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Techno", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(761, 522);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 14);
            this.label5.TabIndex = 33;
            this.label5.Text = "Contrast";
            // 
            // contrastTracker
            // 
            this.contrastTracker.FillColor = System.Drawing.Color.Black;
            this.contrastTracker.HoverState.Parent = this.contrastTracker;
            this.contrastTracker.Location = new System.Drawing.Point(666, 548);
            this.contrastTracker.Maximum = 255;
            this.contrastTracker.Minimum = -255;
            this.contrastTracker.Name = "contrastTracker";
            this.contrastTracker.Size = new System.Drawing.Size(278, 23);
            this.contrastTracker.TabIndex = 32;
            this.contrastTracker.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.contrastTracker.Value = 0;
            this.contrastTracker.ValueChanged += new System.EventHandler(this.contrastTracker_ValueChanged);
            // 
            // brightness
            // 
            this.brightness.AutoSize = true;
            this.brightness.Font = new System.Drawing.Font("Techno", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brightness.Location = new System.Drawing.Point(950, 457);
            this.brightness.Name = "brightness";
            this.brightness.Size = new System.Drawing.Size(22, 20);
            this.brightness.TabIndex = 31;
            this.brightness.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Techno", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(761, 431);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 14);
            this.label3.TabIndex = 30;
            this.label3.Text = "Brightness";
            // 
            // brightnessTracker
            // 
            this.brightnessTracker.FillColor = System.Drawing.Color.Black;
            this.brightnessTracker.HoverState.Parent = this.brightnessTracker;
            this.brightnessTracker.Location = new System.Drawing.Point(666, 457);
            this.brightnessTracker.Maximum = 255;
            this.brightnessTracker.Minimum = -255;
            this.brightnessTracker.Name = "brightnessTracker";
            this.brightnessTracker.Size = new System.Drawing.Size(278, 23);
            this.brightnessTracker.TabIndex = 29;
            this.brightnessTracker.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.brightnessTracker.Value = 0;
            this.brightnessTracker.ValueChanged += new System.EventHandler(this.brightnessTracker_ValueChanged);
            // 
            // percentage
            // 
            this.percentage.AutoSize = true;
            this.percentage.Font = new System.Drawing.Font("Techno", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.percentage.Location = new System.Drawing.Point(950, 381);
            this.percentage.Name = "percentage";
            this.percentage.Size = new System.Drawing.Size(48, 20);
            this.percentage.TabIndex = 28;
            this.percentage.Text = "255";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Techno", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(778, 355);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 14);
            this.label1.TabIndex = 27;
            this.label1.Text = "Opacity";
            // 
            // opacityTracker
            // 
            this.opacityTracker.FillColor = System.Drawing.Color.Black;
            this.opacityTracker.HoverState.Parent = this.opacityTracker;
            this.opacityTracker.Location = new System.Drawing.Point(666, 381);
            this.opacityTracker.Maximum = 255;
            this.opacityTracker.Name = "opacityTracker";
            this.opacityTracker.Size = new System.Drawing.Size(278, 23);
            this.opacityTracker.TabIndex = 26;
            this.opacityTracker.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.opacityTracker.Value = 255;
            this.opacityTracker.ValueChanged += new System.EventHandler(this.opacityTracker_ValueChanged);
            // 
            // btnNegative
            // 
            this.btnNegative.CheckedState.Parent = this.btnNegative;
            this.btnNegative.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNegative.CustomImages.Parent = this.btnNegative;
            this.btnNegative.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnNegative.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnNegative.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnNegative.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnNegative.DisabledState.Parent = this.btnNegative;
            this.btnNegative.FillColor = System.Drawing.Color.DarkRed;
            this.btnNegative.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNegative.ForeColor = System.Drawing.Color.White;
            this.btnNegative.HoverState.Parent = this.btnNegative;
            this.btnNegative.Location = new System.Drawing.Point(666, 248);
            this.btnNegative.Name = "btnNegative";
            this.btnNegative.ShadowDecoration.Parent = this.btnNegative;
            this.btnNegative.Size = new System.Drawing.Size(154, 33);
            this.btnNegative.TabIndex = 25;
            this.btnNegative.Text = "Negativo";
            this.btnNegative.Click += new System.EventHandler(this.btnNegative_Click);
            // 
            // btnSepia
            // 
            this.btnSepia.CheckedState.Parent = this.btnSepia;
            this.btnSepia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSepia.CustomImages.Parent = this.btnSepia;
            this.btnSepia.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnSepia.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnSepia.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnSepia.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnSepia.DisabledState.Parent = this.btnSepia;
            this.btnSepia.FillColor = System.Drawing.Color.DarkRed;
            this.btnSepia.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSepia.ForeColor = System.Drawing.Color.White;
            this.btnSepia.HoverState.Parent = this.btnSepia;
            this.btnSepia.Location = new System.Drawing.Point(843, 178);
            this.btnSepia.Name = "btnSepia";
            this.btnSepia.ShadowDecoration.Parent = this.btnSepia;
            this.btnSepia.Size = new System.Drawing.Size(154, 33);
            this.btnSepia.TabIndex = 24;
            this.btnSepia.Text = "Sepia";
            this.btnSepia.Click += new System.EventHandler(this.btnSepia_Click);
            // 
            // btnGrayScale
            // 
            this.btnGrayScale.CheckedState.Parent = this.btnGrayScale;
            this.btnGrayScale.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGrayScale.CustomImages.Parent = this.btnGrayScale;
            this.btnGrayScale.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnGrayScale.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnGrayScale.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnGrayScale.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnGrayScale.DisabledState.Parent = this.btnGrayScale;
            this.btnGrayScale.FillColor = System.Drawing.Color.DarkRed;
            this.btnGrayScale.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrayScale.ForeColor = System.Drawing.Color.White;
            this.btnGrayScale.HoverState.Parent = this.btnGrayScale;
            this.btnGrayScale.Location = new System.Drawing.Point(666, 178);
            this.btnGrayScale.Name = "btnGrayScale";
            this.btnGrayScale.ShadowDecoration.Parent = this.btnGrayScale;
            this.btnGrayScale.Size = new System.Drawing.Size(154, 33);
            this.btnGrayScale.TabIndex = 23;
            this.btnGrayScale.Text = "Gray";
            this.btnGrayScale.Click += new System.EventHandler(this.btnGrayScale_Click);
            // 
            // btnReset
            // 
            this.btnReset.CheckedState.Parent = this.btnReset;
            this.btnReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReset.CustomImages.Parent = this.btnReset;
            this.btnReset.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnReset.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnReset.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnReset.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnReset.DisabledState.Parent = this.btnReset;
            this.btnReset.FillColor = System.Drawing.Color.DarkRed;
            this.btnReset.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.HoverState.Parent = this.btnReset;
            this.btnReset.Location = new System.Drawing.Point(730, 126);
            this.btnReset.Name = "btnReset";
            this.btnReset.ShadowDecoration.Parent = this.btnReset;
            this.btnReset.Size = new System.Drawing.Size(200, 33);
            this.btnReset.TabIndex = 22;
            this.btnReset.Text = "Reset";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnPrenderCamara
            // 
            this.btnPrenderCamara.CheckedState.Parent = this.btnPrenderCamara;
            this.btnPrenderCamara.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrenderCamara.CustomImages.Parent = this.btnPrenderCamara;
            this.btnPrenderCamara.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnPrenderCamara.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnPrenderCamara.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnPrenderCamara.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnPrenderCamara.DisabledState.Parent = this.btnPrenderCamara;
            this.btnPrenderCamara.FillColor = System.Drawing.Color.DarkRed;
            this.btnPrenderCamara.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrenderCamara.ForeColor = System.Drawing.Color.White;
            this.btnPrenderCamara.HoverState.Parent = this.btnPrenderCamara;
            this.btnPrenderCamara.Location = new System.Drawing.Point(383, 18);
            this.btnPrenderCamara.Name = "btnPrenderCamara";
            this.btnPrenderCamara.ShadowDecoration.Parent = this.btnPrenderCamara;
            this.btnPrenderCamara.Size = new System.Drawing.Size(112, 33);
            this.btnPrenderCamara.TabIndex = 35;
            this.btnPrenderCamara.Text = "Prender";
            this.btnPrenderCamara.Click += new System.EventHandler(this.btnPrenderCamara_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Techno", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(64, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 14);
            this.label2.TabIndex = 36;
            this.label2.Text = "Devices";
            // 
            // btnBinary
            // 
            this.btnBinary.CheckedState.Parent = this.btnBinary;
            this.btnBinary.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBinary.CustomImages.Parent = this.btnBinary;
            this.btnBinary.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnBinary.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnBinary.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnBinary.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnBinary.DisabledState.Parent = this.btnBinary;
            this.btnBinary.FillColor = System.Drawing.Color.DarkRed;
            this.btnBinary.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBinary.ForeColor = System.Drawing.Color.White;
            this.btnBinary.HoverState.Parent = this.btnBinary;
            this.btnBinary.Location = new System.Drawing.Point(843, 248);
            this.btnBinary.Name = "btnBinary";
            this.btnBinary.ShadowDecoration.Parent = this.btnBinary;
            this.btnBinary.Size = new System.Drawing.Size(154, 33);
            this.btnBinary.TabIndex = 37;
            this.btnBinary.Text = "Binary";
            this.btnBinary.Click += new System.EventHandler(this.btnBinary_Click);
            // 
            // BTN_PixelFilter
            // 
            this.BTN_PixelFilter.CheckedState.Parent = this.BTN_PixelFilter;
            this.BTN_PixelFilter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTN_PixelFilter.CustomImages.Parent = this.BTN_PixelFilter;
            this.BTN_PixelFilter.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.BTN_PixelFilter.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.BTN_PixelFilter.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.BTN_PixelFilter.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.BTN_PixelFilter.DisabledState.Parent = this.BTN_PixelFilter;
            this.BTN_PixelFilter.FillColor = System.Drawing.Color.DarkRed;
            this.BTN_PixelFilter.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_PixelFilter.ForeColor = System.Drawing.Color.White;
            this.BTN_PixelFilter.HoverState.Parent = this.BTN_PixelFilter;
            this.BTN_PixelFilter.Location = new System.Drawing.Point(750, 304);
            this.BTN_PixelFilter.Name = "BTN_PixelFilter";
            this.BTN_PixelFilter.ShadowDecoration.Parent = this.BTN_PixelFilter;
            this.BTN_PixelFilter.Size = new System.Drawing.Size(154, 33);
            this.BTN_PixelFilter.TabIndex = 38;
            this.BTN_PixelFilter.Text = "PIXEL";
            this.BTN_PixelFilter.Click += new System.EventHandler(this.BTN_PixelFilter_Click);
            // 
            // Camera
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BTN_PixelFilter);
            this.Controls.Add(this.btnBinary);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnPrenderCamara);
            this.Controls.Add(this.contrast);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.contrastTracker);
            this.Controls.Add(this.brightness);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.brightnessTracker);
            this.Controls.Add(this.percentage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.opacityTracker);
            this.Controls.Add(this.btnNegative);
            this.Controls.Add(this.btnSepia);
            this.Controls.Add(this.btnGrayScale);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.pbCamara);
            this.Controls.Add(this.cbDispositivos);
            this.Name = "Camera";
            this.Size = new System.Drawing.Size(1000, 600);
            this.Load += new System.EventHandler(this.Camera_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbCamara)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2ComboBox cbDispositivos;
        private Guna.UI2.WinForms.Guna2PictureBox pbCamara;
        private System.Windows.Forms.Label contrast;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2TrackBar contrastTracker;
        private System.Windows.Forms.Label brightness;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2TrackBar brightnessTracker;
        private System.Windows.Forms.Label percentage;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2TrackBar opacityTracker;
        private Guna.UI2.WinForms.Guna2Button btnNegative;
        private Guna.UI2.WinForms.Guna2Button btnSepia;
        private Guna.UI2.WinForms.Guna2Button btnGrayScale;
        private Guna.UI2.WinForms.Guna2Button btnReset;
        private Guna.UI2.WinForms.Guna2Button btnPrenderCamara;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2Button btnBinary;
        private Guna.UI2.WinForms.Guna2Button BTN_PixelFilter;
    }
}
