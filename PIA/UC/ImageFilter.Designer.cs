﻿
namespace PIA.UC
{
    partial class ImageFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pbImage = new System.Windows.Forms.PictureBox();
            this.btnUploadFile = new Guna.UI2.WinForms.Guna2Button();
            this.guna2BorderlessForm1 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.btnSaveImage = new Guna.UI2.WinForms.Guna2Button();
            this.btnReset = new Guna.UI2.WinForms.Guna2Button();
            this.btnGrayScale = new Guna.UI2.WinForms.Guna2Button();
            this.btnSepia = new Guna.UI2.WinForms.Guna2Button();
            this.btnNegative = new Guna.UI2.WinForms.Guna2Button();
            this.guna2ColorTransition1 = new Guna.UI2.WinForms.Guna2ColorTransition(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.opacityTracker = new Guna.UI2.WinForms.Guna2TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.percentage = new System.Windows.Forms.Label();
            this.brightness = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.brightnessTracker = new Guna.UI2.WinForms.Guna2TrackBar();
            this.contrast = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.contrastTracker = new Guna.UI2.WinForms.Guna2TrackBar();
            this.btnBinary = new Guna.UI2.WinForms.Guna2Button();
            this.BTN_Pixel = new Guna.UI2.WinForms.Guna2Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pbImage
            // 
            this.pbImage.Location = new System.Drawing.Point(15, 15);
            this.pbImage.Name = "pbImage";
            this.pbImage.Size = new System.Drawing.Size(612, 466);
            this.pbImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbImage.TabIndex = 0;
            this.pbImage.TabStop = false;
            // 
            // btnUploadFile
            // 
            this.btnUploadFile.CheckedState.Parent = this.btnUploadFile;
            this.btnUploadFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUploadFile.CustomImages.Parent = this.btnUploadFile;
            this.btnUploadFile.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnUploadFile.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnUploadFile.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnUploadFile.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnUploadFile.DisabledState.Parent = this.btnUploadFile;
            this.btnUploadFile.FillColor = System.Drawing.Color.Black;
            this.btnUploadFile.Font = new System.Drawing.Font("Techno", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadFile.ForeColor = System.Drawing.Color.White;
            this.btnUploadFile.HoverState.Parent = this.btnUploadFile;
            this.btnUploadFile.Location = new System.Drawing.Point(41, 509);
            this.btnUploadFile.Name = "btnUploadFile";
            this.btnUploadFile.ShadowDecoration.Parent = this.btnUploadFile;
            this.btnUploadFile.Size = new System.Drawing.Size(174, 45);
            this.btnUploadFile.TabIndex = 2;
            this.btnUploadFile.Text = "Upload";
            this.btnUploadFile.Click += new System.EventHandler(this.btnUploadFile_Click);
            // 
            // guna2BorderlessForm1
            // 
            this.guna2BorderlessForm1.ContainerControl = this;
            // 
            // btnSaveImage
            // 
            this.btnSaveImage.CheckedState.Parent = this.btnSaveImage;
            this.btnSaveImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveImage.CustomImages.Parent = this.btnSaveImage;
            this.btnSaveImage.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnSaveImage.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnSaveImage.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnSaveImage.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnSaveImage.DisabledState.Parent = this.btnSaveImage;
            this.btnSaveImage.FillColor = System.Drawing.Color.Black;
            this.btnSaveImage.Font = new System.Drawing.Font("Techno", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveImage.ForeColor = System.Drawing.Color.White;
            this.btnSaveImage.HoverState.Parent = this.btnSaveImage;
            this.btnSaveImage.Location = new System.Drawing.Point(400, 509);
            this.btnSaveImage.Name = "btnSaveImage";
            this.btnSaveImage.ShadowDecoration.Parent = this.btnSaveImage;
            this.btnSaveImage.Size = new System.Drawing.Size(174, 45);
            this.btnSaveImage.TabIndex = 3;
            this.btnSaveImage.Text = "Save";
            this.btnSaveImage.Click += new System.EventHandler(this.btnSaveImage_Click);
            // 
            // btnReset
            // 
            this.btnReset.CheckedState.Parent = this.btnReset;
            this.btnReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReset.CustomImages.Parent = this.btnReset;
            this.btnReset.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnReset.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnReset.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnReset.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnReset.DisabledState.Parent = this.btnReset;
            this.btnReset.FillColor = System.Drawing.Color.DarkRed;
            this.btnReset.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.HoverState.Parent = this.btnReset;
            this.btnReset.Location = new System.Drawing.Point(720, 15);
            this.btnReset.Name = "btnReset";
            this.btnReset.ShadowDecoration.Parent = this.btnReset;
            this.btnReset.Size = new System.Drawing.Size(200, 33);
            this.btnReset.TabIndex = 4;
            this.btnReset.Text = "Reset";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnGrayScale
            // 
            this.btnGrayScale.CheckedState.Parent = this.btnGrayScale;
            this.btnGrayScale.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGrayScale.CustomImages.Parent = this.btnGrayScale;
            this.btnGrayScale.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnGrayScale.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnGrayScale.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnGrayScale.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnGrayScale.DisabledState.Parent = this.btnGrayScale;
            this.btnGrayScale.FillColor = System.Drawing.Color.DarkRed;
            this.btnGrayScale.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrayScale.ForeColor = System.Drawing.Color.White;
            this.btnGrayScale.HoverState.Parent = this.btnGrayScale;
            this.btnGrayScale.Location = new System.Drawing.Point(643, 127);
            this.btnGrayScale.Name = "btnGrayScale";
            this.btnGrayScale.ShadowDecoration.Parent = this.btnGrayScale;
            this.btnGrayScale.Size = new System.Drawing.Size(154, 33);
            this.btnGrayScale.TabIndex = 5;
            this.btnGrayScale.Text = "Gray";
            this.btnGrayScale.Click += new System.EventHandler(this.btnGrayScale_Click);
            // 
            // btnSepia
            // 
            this.btnSepia.CheckedState.Parent = this.btnSepia;
            this.btnSepia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSepia.CustomImages.Parent = this.btnSepia;
            this.btnSepia.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnSepia.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnSepia.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnSepia.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnSepia.DisabledState.Parent = this.btnSepia;
            this.btnSepia.FillColor = System.Drawing.Color.DarkRed;
            this.btnSepia.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSepia.ForeColor = System.Drawing.Color.White;
            this.btnSepia.HoverState.Parent = this.btnSepia;
            this.btnSepia.Location = new System.Drawing.Point(843, 127);
            this.btnSepia.Name = "btnSepia";
            this.btnSepia.ShadowDecoration.Parent = this.btnSepia;
            this.btnSepia.Size = new System.Drawing.Size(154, 33);
            this.btnSepia.TabIndex = 6;
            this.btnSepia.Text = "Sepia";
            this.btnSepia.Click += new System.EventHandler(this.btnSepia_Click);
            // 
            // btnNegative
            // 
            this.btnNegative.CheckedState.Parent = this.btnNegative;
            this.btnNegative.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNegative.CustomImages.Parent = this.btnNegative;
            this.btnNegative.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnNegative.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnNegative.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnNegative.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnNegative.DisabledState.Parent = this.btnNegative;
            this.btnNegative.FillColor = System.Drawing.Color.DarkRed;
            this.btnNegative.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNegative.ForeColor = System.Drawing.Color.White;
            this.btnNegative.HoverState.Parent = this.btnNegative;
            this.btnNegative.Location = new System.Drawing.Point(643, 201);
            this.btnNegative.Name = "btnNegative";
            this.btnNegative.ShadowDecoration.Parent = this.btnNegative;
            this.btnNegative.Size = new System.Drawing.Size(154, 33);
            this.btnNegative.TabIndex = 7;
            this.btnNegative.Text = "Negativo";
            this.btnNegative.Click += new System.EventHandler(this.btnNegative_Click);
            // 
            // guna2ColorTransition1
            // 
            this.guna2ColorTransition1.ColorArray = new System.Drawing.Color[] {
        System.Drawing.Color.Red,
        System.Drawing.Color.Blue,
        System.Drawing.Color.Orange};
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // opacityTracker
            // 
            this.opacityTracker.FillColor = System.Drawing.Color.Black;
            this.opacityTracker.HoverState.Parent = this.opacityTracker;
            this.opacityTracker.Location = new System.Drawing.Point(652, 341);
            this.opacityTracker.Maximum = 255;
            this.opacityTracker.Name = "opacityTracker";
            this.opacityTracker.Size = new System.Drawing.Size(278, 23);
            this.opacityTracker.TabIndex = 13;
            this.opacityTracker.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.opacityTracker.Value = 255;
            this.opacityTracker.ValueChanged += new System.EventHandler(this.opacityTracker_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Techno", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(765, 315);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 14);
            this.label1.TabIndex = 14;
            this.label1.Text = "Opacity";
            // 
            // percentage
            // 
            this.percentage.AutoSize = true;
            this.percentage.Font = new System.Drawing.Font("Techno", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.percentage.Location = new System.Drawing.Point(934, 341);
            this.percentage.Name = "percentage";
            this.percentage.Size = new System.Drawing.Size(48, 20);
            this.percentage.TabIndex = 15;
            this.percentage.Text = "255";
            // 
            // brightness
            // 
            this.brightness.AutoSize = true;
            this.brightness.Font = new System.Drawing.Font("Techno", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brightness.Location = new System.Drawing.Point(934, 416);
            this.brightness.Name = "brightness";
            this.brightness.Size = new System.Drawing.Size(22, 20);
            this.brightness.TabIndex = 18;
            this.brightness.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Techno", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(747, 390);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Brightness";
            // 
            // brightnessTracker
            // 
            this.brightnessTracker.FillColor = System.Drawing.Color.Black;
            this.brightnessTracker.HoverState.Parent = this.brightnessTracker;
            this.brightnessTracker.Location = new System.Drawing.Point(652, 416);
            this.brightnessTracker.Maximum = 255;
            this.brightnessTracker.Minimum = -255;
            this.brightnessTracker.Name = "brightnessTracker";
            this.brightnessTracker.Size = new System.Drawing.Size(278, 23);
            this.brightnessTracker.TabIndex = 16;
            this.brightnessTracker.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.brightnessTracker.Value = 0;
            this.brightnessTracker.ValueChanged += new System.EventHandler(this.brightnessTracker_ValueChanged);
            // 
            // contrast
            // 
            this.contrast.AutoSize = true;
            this.contrast.Font = new System.Drawing.Font("Techno", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contrast.Location = new System.Drawing.Point(934, 509);
            this.contrast.Name = "contrast";
            this.contrast.Size = new System.Drawing.Size(22, 20);
            this.contrast.TabIndex = 21;
            this.contrast.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Techno", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(747, 483);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 14);
            this.label5.TabIndex = 20;
            this.label5.Text = "Contrast";
            // 
            // contrastTracker
            // 
            this.contrastTracker.FillColor = System.Drawing.Color.Black;
            this.contrastTracker.HoverState.Parent = this.contrastTracker;
            this.contrastTracker.Location = new System.Drawing.Point(652, 509);
            this.contrastTracker.Maximum = 255;
            this.contrastTracker.Minimum = -255;
            this.contrastTracker.Name = "contrastTracker";
            this.contrastTracker.Size = new System.Drawing.Size(278, 23);
            this.contrastTracker.TabIndex = 19;
            this.contrastTracker.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.contrastTracker.Value = 0;
            this.contrastTracker.ValueChanged += new System.EventHandler(this.contrastTracker_ValueChanged);
            // 
            // btnBinary
            // 
            this.btnBinary.CheckedState.Parent = this.btnBinary;
            this.btnBinary.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBinary.CustomImages.Parent = this.btnBinary;
            this.btnBinary.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnBinary.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnBinary.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnBinary.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnBinary.DisabledState.Parent = this.btnBinary;
            this.btnBinary.FillColor = System.Drawing.Color.DarkRed;
            this.btnBinary.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBinary.ForeColor = System.Drawing.Color.White;
            this.btnBinary.HoverState.Parent = this.btnBinary;
            this.btnBinary.Location = new System.Drawing.Point(843, 201);
            this.btnBinary.Name = "btnBinary";
            this.btnBinary.ShadowDecoration.Parent = this.btnBinary;
            this.btnBinary.Size = new System.Drawing.Size(154, 33);
            this.btnBinary.TabIndex = 22;
            this.btnBinary.Text = "Binary";
            this.btnBinary.Click += new System.EventHandler(this.btnColdFilter_Click);
            // 
            // BTN_Pixel
            // 
            this.BTN_Pixel.CheckedState.Parent = this.BTN_Pixel;
            this.BTN_Pixel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BTN_Pixel.CustomImages.Parent = this.BTN_Pixel;
            this.BTN_Pixel.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.BTN_Pixel.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.BTN_Pixel.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.BTN_Pixel.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.BTN_Pixel.DisabledState.Parent = this.BTN_Pixel;
            this.BTN_Pixel.FillColor = System.Drawing.Color.DarkRed;
            this.BTN_Pixel.Font = new System.Drawing.Font("Techno", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_Pixel.ForeColor = System.Drawing.Color.White;
            this.BTN_Pixel.HoverState.Parent = this.BTN_Pixel;
            this.BTN_Pixel.Location = new System.Drawing.Point(738, 259);
            this.BTN_Pixel.Name = "BTN_Pixel";
            this.BTN_Pixel.ShadowDecoration.Parent = this.BTN_Pixel;
            this.BTN_Pixel.Size = new System.Drawing.Size(154, 33);
            this.BTN_Pixel.TabIndex = 23;
            this.BTN_Pixel.Text = "PIXEL";
            this.BTN_Pixel.Click += new System.EventHandler(this.BTN_Pixel_Click);
            // 
            // ImageFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BTN_Pixel);
            this.Controls.Add(this.btnBinary);
            this.Controls.Add(this.contrast);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.contrastTracker);
            this.Controls.Add(this.brightness);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.brightnessTracker);
            this.Controls.Add(this.percentage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.opacityTracker);
            this.Controls.Add(this.btnNegative);
            this.Controls.Add(this.btnSepia);
            this.Controls.Add(this.btnGrayScale);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSaveImage);
            this.Controls.Add(this.btnUploadFile);
            this.Controls.Add(this.pbImage);
            this.Name = "ImageFilter";
            this.Size = new System.Drawing.Size(1000, 600);
            ((System.ComponentModel.ISupportInitialize)(this.pbImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbImage;
        private Guna.UI2.WinForms.Guna2Button btnUploadFile;
        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm1;
        private Guna.UI2.WinForms.Guna2Button btnSaveImage;
        private Guna.UI2.WinForms.Guna2Button btnReset;
        private Guna.UI2.WinForms.Guna2Button btnNegative;
        private Guna.UI2.WinForms.Guna2Button btnSepia;
        private Guna.UI2.WinForms.Guna2Button btnGrayScale;
        private Guna.UI2.WinForms.Guna2ColorTransition guna2ColorTransition1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label percentage;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2TrackBar opacityTracker;
        private System.Windows.Forms.Label contrast;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2TrackBar contrastTracker;
        private System.Windows.Forms.Label brightness;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2TrackBar brightnessTracker;
        private Guna.UI2.WinForms.Guna2Button btnBinary;
        private Guna.UI2.WinForms.Guna2Button BTN_Pixel;
    }
}
