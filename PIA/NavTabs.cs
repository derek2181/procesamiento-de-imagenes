﻿using PIA.UC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PIA
{
    public partial class NavTabs : Form
    {
        public NavTabs()
        {
            InitializeComponent();
            UC.ImageFilter control = new UC.ImageFilter();
            ChangeControl(control);
            guna2Button1.PerformClick();
        }

        private void ChangeControl(UserControl userControl)
        {
            userControl.Dock = DockStyle.Fill;
            PanelContainer.Controls.Clear();
            PanelContainer.Controls.Add(userControl);
            userControl.BringToFront();
        }
        private void guna2Button1_Click(object sender, EventArgs e)
        {
            UC.ImageFilter control = new UC.ImageFilter();
            ChangeControl(control);
            try
            {
                if(Camera.webcam!=null)
                if (Camera.webcam.IsRunning)
                    Camera.webcam.Stop();
            }
            catch (Exception)
            {
               
            }
          
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            UC.Camera control = new UC.Camera();
            ChangeControl(control);
        }

        private void PanelContainer_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {

        }
    }
}
